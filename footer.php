<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.scrollex.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.scrolly.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/browser.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/breakpoints.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/util.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>

$("#submit-order-btn").click(function() {
    if(document.getElementById('name').value == "") {
        swal('กรุณาระบุ ชื่อ - สกุล');
        return false;
    }
    if(document.getElementById('tel').value == "") {
        swal('กรุณาระบุ เบอร์โทรศัพท์');
        return false;
    }
    if(document.getElementById('email').value == "") {
        swal('กรุณาระบุ อีเมล์');
        return false;
    }
    if(document.getElementById('product').value == 0) {
        
        swal('กรุณาระบุ สินค้าที่ต้องการ');
        return false;
    }
    if(document.getElementById('count').value == "") {
        swal('กรุณาระบุ จำนวนสินค้า');
        return false;
    }
    if(document.getElementById('address').value == "") {
        swal('กรุณาระบุ ที่อยู่จัดส่งสินค้า');
        return false;
    }
    swal({
        buttons : false,
        title: "ระบบกำลังทำรายการ",
        text: "กรุณารอสักครู่...",

  closeOnClickOutside: false,
});
    
    var url= "<?php echo get_template_directory_uri() ?>/add-order.php"; 
        var dataSet = {
            name: $("input#name").val(),
            tel: $("input#tel").val(),
            email: $("input#email").val(),
            product: $("#product").val(),
            count: $("input#count").val(),
            address_customer: $("#address").val()
            }; 
        $.post(url,dataSet,function(data){

            if(data == 'success'){
                swal.close();
                swal("ทำรายการสำเร็จ", "เจ้าหน้าที่จะติดต่อกลับในไม่ช้า", "success");
            }
         
         });
});

</script>